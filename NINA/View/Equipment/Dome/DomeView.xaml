<!--
    Copyright © 2016 - 2020 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.-->
<UserControl
    x:Class="NINA.View.Equipment.DomeView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:equip="clr-namespace:NINA.View.Equipment"
    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
    xmlns:rules="clr-namespace:NINA.Utility.ValidationRules"
    xmlns:ns="clr-namespace:NINA.Locale"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition />
            <ColumnDefinition />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition />
            <RowDefinition />
        </Grid.RowDefinitions>
        <Grid Grid.ColumnSpan="1">
            <GroupBox>
                <GroupBox.Header>
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="3*" />
                        </Grid.ColumnDefinitions>
                        <TextBlock
                        VerticalAlignment="Center"
                        FontSize="20"
                        Text="{ns:Loc LblDome}" />
                        <equip:Connector
                        Grid.Column="1"
                        CancelCommand="{Binding CancelChooseDomeCommand}"
                        ConnectCommand="{Binding ChooseDomeCommand}"
                        Connected="{Binding DomeInfo.Connected}"
                        Devices="{Binding DomeChooserVM.Devices}"
                        DisconnectCommand="{Binding DisconnectCommand}"
                        HasSetupDialog="{Binding DomeChooserVM.SelectedDevice.HasSetupDialog}"
                        RefreshCommand="{Binding RefreshDomeListCommand}"
                        SelectedDevice="{Binding DomeChooserVM.SelectedDevice, Mode=TwoWay}"
                        SetupCommand="{Binding DomeChooserVM.SetupDialogCommand}" />
                    </Grid>
                </GroupBox.Header>

                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="*" />
                    </Grid.RowDefinitions>
                    <StackPanel Margin="0,0,0,20" Grid.Row="0">
                        <Border
                        BorderBrush="{StaticResource BorderBrush}"
                        BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                Margin="0,6,0,6"
                                VerticalAlignment="Center"
                                Columns="2">
                                    <TextBlock Text="{ns:Loc LblName}" />
                                    <TextBlock
                                    Margin="5,0,0,0"
                                    Text="{Binding Dome.Name}"
                                    TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                Margin="0,6,0,6"
                                VerticalAlignment="Center"
                                Columns="2">
                                    <TextBlock Text="{ns:Loc LblDescription}" />
                                    <TextBlock
                                    Margin="5,0,0,0"
                                    Text="{Binding Dome.Description}"
                                    TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                Margin="0,6,0,6"
                                VerticalAlignment="Center"
                                Columns="2">
                                    <TextBlock Text="{ns:Loc LblDriverInfo}" />
                                    <TextBlock
                                    Margin="5,0,0,0"
                                    Text="{Binding Dome.DriverInfo}"
                                    TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                                <UniformGrid
                                Margin="0,6,0,6"
                                VerticalAlignment="Center"
                                Columns="2">
                                    <TextBlock Text="{ns:Loc LblDriverVersion}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding Dome.DriverVersion}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                Margin="0,6,0,6"
                                VerticalAlignment="Center"
                                Columns="2">
                                    <TextBlock Text="{ns:Loc LblDomeAzimuth}" />
                                    <TextBlock
                                    Margin="5,0,0,0"
                                    Text="{Binding DomeInfo.Azimuth}"
                                    TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                                <UniformGrid
                                Margin="0,6,0,6"
                                VerticalAlignment="Center"
                                Columns="2"
                                Visibility="{Binding DomeInfo.ShutterStatus, Converter={StaticResource ShutterStatusToVisibilityConverter}}">
                                    <TextBlock Text="{ns:Loc LblDomeShutterStatus}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding DomeInfo.ShutterStatus}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                    </StackPanel>
                </Grid>
            </GroupBox>
        </Grid>
        <Grid Grid.Row="1" IsEnabled="{Binding Dome, Converter={StaticResource InverseNullToBooleanConverter}}" Grid.Column="0">
            <Grid IsEnabled="{Binding DomeInfo.Connected}">
                <GroupBox Header="{ns:Loc LblDomeSynchronization}">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="200" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="50" />
                            <RowDefinition Height="*" />
                        </Grid.RowDefinitions>
                        <Grid Grid.Row="0" Grid.Column="0">
                            
                        </Grid>
                    </Grid>
                </GroupBox>
            </Grid>
        </Grid>
        <Grid
            Grid.Row="1"
            Grid.Column="1"
            IsEnabled="{Binding Dome, Converter={StaticResource InverseNullToBooleanConverter}}">
            <Grid IsEnabled="{Binding DomeInfo.Connected}">
                <GroupBox
                    Grid.Row="1"
                    Header="{ns:Loc LblManualControl}">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="100" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="*" />
                            <RowDefinition Height="100" />
                            <RowDefinition Height="100" />
                            <RowDefinition Height="50" />
                        </Grid.RowDefinitions>
                        <Grid Grid.Row="3" Grid.Column="1">
                            <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                                <StackPanel Orientation="Horizontal" HorizontalAlignment="Right" IsEnabled="{Binding DomeInfo.CanSetAzimuth}">
                                    <Button
                                        Width="40"
                                        Height="40"
                                        Margin="5"
                                        HorizontalAlignment="Right"
                                        Command="{Binding ManualSlewCommand}">
                                        <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblSlew}"/>
                                    </Button>
                                    <TextBox
                                        MinWidth="30"
                                        Margin="5,0,0,0"
                                        TextAlignment="Right"
                                        VerticalAlignment="Center">
                                        <TextBox.Text>
                                            <Binding Path="TargetAzimuthDegrees" UpdateSourceTrigger="LostFocus">
                                                <Binding.ValidationRules>
                                                    <rules:FullCircleDegreesRule />
                                                </Binding.ValidationRules>
                                            </Binding>
                                        </TextBox.Text>
                                    </TextBox>
                                    <TextBlock Margin="0,-10,0,0" VerticalAlignment="Center" Text="&#730;"/>
                                </StackPanel>
                            </Border>
                        </Grid>
                        <Button
                            Grid.Row="1"
                            Grid.Column="0"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Right"
                            IsEnabled="{Binding DomeInfo.CanSetAzimuth}">
                            <!-- TODO: Isbeorn, can you make a Counter-Clockwise logo? -->
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="CCW" />
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="PreviewStylusDown">
                                    <i:InvokeCommandAction Command="{Binding DomeRotateCommand}" CommandParameter="CCW" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewStylusUp">
                                    <i:InvokeCommandAction Command="{Binding StopDomeRotateCommand}" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchDown">
                                    <i:InvokeCommandAction Command="{Binding DomeRotateCommand}" CommandParameter="CCW" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchUp">
                                    <i:InvokeCommandAction Command="{Binding StopDomeRotateCommand}" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonDown">
                                    <i:InvokeCommandAction Command="{Binding DomeRotateCommand}" CommandParameter="CCW" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonUp">
                                    <i:InvokeCommandAction Command="{Binding StopDomeRotateCommand}" />
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </Button>
                        <Button
                            Grid.Row="1"
                            Grid.Column="2"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Left"
                            IsEnabled="{Binding DomeInfo.CanSetAzimuth}">
                            <!-- TODO: Isbeorn, can you make a Clockwise logo? -->
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="CW" />
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="PreviewStylusDown">
                                    <i:InvokeCommandAction Command="{Binding DomeRotateCommand}" CommandParameter="CW" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewStylusUp">
                                    <i:InvokeCommandAction Command="{Binding StopDomeRotateCommand}" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchDown">
                                    <i:InvokeCommandAction Command="{Binding DomeRotateCommand}" CommandParameter="CW" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchUp">
                                    <i:InvokeCommandAction Command="{Binding StopDomeRotateCommand}" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonDown">
                                    <i:InvokeCommandAction Command="{Binding DomeRotateCommand}" CommandParameter="CW" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonUp">
                                    <i:InvokeCommandAction Command="{Binding StopDomeRotateCommand}" />
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </Button>
                        <Button
                            Grid.Row="0"
                            Grid.Column="1"
                            Width="80"
                            Height="80"
                            Command="{Binding OpenShutterCommand}"
                            VerticalAlignment="Bottom"
                            IsEnabled="{Binding DomeInfo.CanSetShutter}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblDomeShutterOpen}" />
                        </Button>
                        <Button
                            Grid.Row="2"
                            Grid.Column="1"
                            Width="80"
                            Height="80"
                            Command="{Binding CloseShutterCommand}"
                            VerticalAlignment="Top"
                            IsEnabled="{Binding DomeInfo.CanSetShutter}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblDomeShutterClose}" />
                        </Button>
                        <Button
                            Grid.Row="1"
                            Grid.Column="1"
                            Width="80"
                            Height="80"
                            Command="{Binding StopCommand}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblStop}" />
                        </Button>

                        <Button
                            Grid.Row="2"
                            Grid.Column="3"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Right"
                            Command="{Binding ParkCommand}"
                            IsEnabled="{Binding DomeInfo.CanPark}"
                            Visibility="{Binding DomeInfo.AtPark, Converter={StaticResource InverseBoolToVisibilityConverter}}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblPark}" />
                        </Button>
                        <Button
                            Grid.Row="3"
                            Grid.Column="3"
                            Width="80"
                            Height="40"
                            HorizontalAlignment="Right"
                            Command="{Binding SetParkPositionCommand}"
                            IsEnabled="{Binding DomeInfo.AtPark, Converter={StaticResource InverseBooleanConverter}}"
                            Visibility="{Binding DomeInfo.CanSetPark, Converter={StaticResource VisibilityConverter}}">
                            <Button.ToolTip>
                                <ToolTip ToolTipService.ShowOnDisabled="True">
                                    <TextBlock Text="{ns:Loc LblSetParkToolTip}" />
                                </ToolTip>
                            </Button.ToolTip>
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblSetPark}" />
                        </Button>
                    </Grid>
                </GroupBox>
            </Grid>
        </Grid>
    </Grid>
</UserControl>