﻿using NINA.Model.MyDome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NINA.Utility.Mediator.Interfaces {
    public interface IDomeConsumer : IDeviceConsumer<DomeInfo> {
    }
}
